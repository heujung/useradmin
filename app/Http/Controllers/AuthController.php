<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterFormRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * 회원가입
     *
     * @param RegisterFormRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function register(RegisterFormRequest $request)
    {
        $user = new User;

        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();

        return response([
            'status' => 'success',
            'data' => $user
        ], Response::HTTP_OK);
    }

    /**
     * 로그인
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => '인증 실패'
            ], Response::HTTP_BAD_REQUEST);
        }

        return response([
            'status' => 'success'
        ], Response::HTTP_OK)->header('Authorization', $token);
    }

    /**
     * 로그아웃
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function logout()
    {
        JWTAuth::invalidate();

        return response([
            'status' => 'success',
            'msg' => '로그아웃 성공'
        ], Response::HTTP_OK);
    }

    /* JWT 인증 관련 */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response([
            'status' => 'success',
            'data' => $user
        ], Response::HTTP_OK);
    }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ], Response::HTTP_OK);
    }
}
