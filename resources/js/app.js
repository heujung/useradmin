
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VModal from 'vue-js-modal';

import App from './App.vue';
import Dashboard from './components/Dashboard.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import UserEdit from './components/UserEdit'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VModal, {
    dynamic: true,
    injectModalsContainer: true,
    dialog: true
});

// Vue Router
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                auth: true
            }
        },{
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },{
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },{
            path: '/user/:id',
            name: 'users.edit',
            component: UserEdit,
            meta: {
                auth: true
            }
        }
    ]
});

// Vue Auth
Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
App.router = Vue.router;

const app = new Vue({
    el: '#app',
    router: router,
    render: app => app(App)
});
