<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{vuerouter}', function () {
//    return view('welcome');
    return view('index');
})->where('vuerouter', '|login|register|user|user\/.*');

// 회원가입
Route::post('auth/register', 'AuthController@register');

// 로그인
Route::post('auth/login', 'AuthController@login');

// JWT
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');

    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@edit');
    Route::post('users/{id}', 'UserController@update');
    Route::delete('users/{id}', 'UserController@destroy');
});
Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});

